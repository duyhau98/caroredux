import * as types from '../helpers/helper'

export const squares = (history, stepNumber, xIsNext) => ({
    type: types.CLICK_SQUARE,
    history, stepNumber, xIsNext
});

export const steps = (stepNumber, xIsNext) => ({
    type: types.SET_STEPS,
    stepNumber, xIsNext
  })

export const increase = (isIncrease) => ({
    type: types.SET_ISINCREASE,
    isIncrease
})