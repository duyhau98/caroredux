import React from 'react';
import './App.css';
import Game from './components/Game';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <div className="Title-header">Game caro Việt Nam</div>
          <Game/>
      </header>
    </div>
  );
}

export default App;
