import * as types from '../helpers/helper';

const initialState = {
    history: [{
      squares: Array(400).fill(null)
    }],
    stepNumber: 0,
    xIsNext: true,
    isIncrease: true
  };

const myReducer = (state = initialState, action) =>{
    switch (action.type) {
        case types.CLICK_SQUARE:

            return {...state,
                history: action.history,
                stepNumber: action.stepNumber,
                xIsNext: action.xIsNext,
            };
        case types.SET_STEPS:
            return {...state,
            stepNumber: action.stepNumber,
            xIsNext: action.xIsNext
        }
        case types.SET_ISINCREASE:
                console.log(action);
            return {...state,
                isIncrease: action.isIncrease

            }
        default: return state;
    }
}
export default myReducer;