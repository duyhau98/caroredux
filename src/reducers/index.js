import { combineReducers } from 'redux';
import games from './game';

const myReducer = combineReducers ({
    games
});
export default myReducer;